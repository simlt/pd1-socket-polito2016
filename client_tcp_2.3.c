#include "lib/mylib.h"

#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>


#define MAX_FILENAME    100
#define BUFSIZE         MAX_FILENAME + 20
#define OK_HEADER       5+4+4


char *prog_name; // for errlib

static const char GET[] = "GET ";
static const char OK[] = "+OK";
static const char ERR[] = "-ERR";


void client_2_3(int);
void getFile(int s, const char* filename);


int main (int argc, char** argv)
{
    int s;
    struct sockaddr_in saddr;
    size_t addrlen;

    prog_name = argv[0];

    if (argc != 3)
    {
        printf("%s <dotted-address> <port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    s = Socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    fillSockaddr(&saddr, argv[1], atoi(argv[2]));
    addrlen = sizeof(saddr);
    Connect(s, (struct sockaddr*)&saddr, addrlen);
    printf("Connected!\n");
    client_2_3(s);
    Close(s);
    return EXIT_SUCCESS;
}


void getFile(int s, const char* filename)
{
    char resp_buffer[BUFSIZE];
    ssize_t r;
    uint32_t data;
    size_t bytes;
    time_t timestamp;
    int fd;

    // Get header
    r = Readline_unbuffered(s, resp_buffer, BUFSIZE);
    if (r == 0)
    {
        puts("Connection closed by server\n");
        exit(EXIT_FAILURE);
    }

    if (strncmp(resp_buffer, OK, strlen(OK)) != 0)
    {
        puts("File not found.\n");
        return;
    }

    // Read remaining part of header
    r = Readn(s, resp_buffer, sizeof(uint32_t)*2);

    data = *(uint32_t*)(resp_buffer);
    bytes = (size_t)ntohl(data);

    data = *(uint32_t*)(resp_buffer+4);
    timestamp = (time_t)ntohl(data);

    // get fd for file output
    fd = creat(filename, 0644);

    if (my_recvfile(s, fd, bytes) < 0)
    {
        printf("File transfer failed.\n");
        return;
    }
    printf("Transfer complete. Written %zu bytes of data. Timestamp: %s\n", bytes, ctime(&timestamp));
}


void client_2_3(int s)
{
    char filename[MAX_FILENAME];
    char buffer[BUFSIZE];

    printf("Client connected to server. Enter list of files:\n");
    printf("\n> ");
    fflush(stdout);

    while (1)
    {
        // Check if input from stdio is ready
        if (fgets(filename, MAX_FILENAME, stdin) == NULL)
            continue;
        if (sscanf(filename, "%s", filename) != 1)
        {
            printf("Invalid format for name\n");
            continue;
        }

        // Send request for file
        sprintf(buffer, "%s%s\r\n", GET, filename);
        Send(s, buffer, strlen(buffer), 0);

        // parse received data
        getFile(s, filename);

        printf("\n> ");
        fflush(stdout);
    }
}
