#ifndef _CLIENT_FILE_H
#define _CLIENT_FILE_H

#include "lib/mylib.h"


// Multiplexing client
void client(int, int xdr);

// return response status
int parseResponse(int s, struct transfer* t);
int parseResponse_xdr(int s, struct transfer* t);
int parseCommand(char* filename);

// move contents to beginning and update offset after using some data
void consumeBuffer(char* buffer, size_t* ready, size_t size);

struct transfer* queueTransfer(int socket, const char* filename);
void requestTransfer(struct transfer* t);


#endif
