#include "lib/mylib.h"
#include "lib/sockwrap.h"
#include "service.h"

#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>


#define LISTEN_BACKLOG  128     // Maximum requests to accept
#define SERVICE_TIMEOUT 5*60    // Timeout in seconds
#define MAX_SERVICE     3       // Max number of clients to serve concurrently


char *prog_name; // for errlib


int main(int argc, char** argv)
{
    int tcp_socket, s;
    struct sockaddr_in saddr;
    socklen_t addrlen;
    int port;
    int i;

    prog_name = argv[0];

    if (argc != 2)
    {
        printf("Usage: %s port\n", argv[0]);
        return -1;
    }
    port = atoi(argv[1]);
    fillSockaddr(&saddr, "0.0.0.0", port);
    tcp_socket = Socket(AF_INET, SOCK_STREAM, 0);
    // Bind to address
    Bind(tcp_socket, (struct sockaddr*)&saddr, sizeof(saddr));
    // Listen
    Listen(tcp_socket, LISTEN_BACKLOG);
    printf("Server started on port %d\n", port);

    // Fork child servers (pre-allocate)
    for (i = 0; i < MAX_SERVICE; ++i)
    {
        if (fork() == 0)
        {
            // CHILD
            // main server loop
            while (1)
            {
                addrlen = sizeof(saddr);
                // Wait for a connection
                s = accept(tcp_socket, (struct sockaddr*)&saddr, &addrlen);
                if (s < 0)
                    continue;

                // Service until timeout
                while (1)
                {
                    if (my_select_timeout(s, SERVICE_TIMEOUT) > 0)
                    {
                        if (service(s) >= 0)
                            continue; // OK
                    }
                    else
                        printf("Client connection timed out\n");

                    // exit and close connection
                    close(s);
                    break; // Wait for a new connection
                }
            }
        }
    }
    // after fork only parent reaches this point
    // PARENT
    close(tcp_socket);
    // Wait for childs to terminate
    // We could also let the init process take care of child process exit status
    for (i = 0; i < MAX_SERVICE; ++i)
        wait(NULL);
    return EXIT_SUCCESS;
}

