#include "client.h"

#include <unistd.h>
#include <stdlib.h>

char *prog_name; // for errlib


int main (int argc, char** argv)
{
    int s;
    struct sockaddr_in saddr;
    size_t addrlen;

    prog_name = argv[0];

    if (argc != 3)
    {
        printf("%s <dotted-address> <port>\n", argv[0]);
        return EXIT_FAILURE;
    }
    s = Socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    fillSockaddr(&saddr, argv[1], atoi(argv[2]));
    addrlen = sizeof(saddr);
    Connect(s, (struct sockaddr*)&saddr, addrlen);
    printf("Connected!\n");
    client(s, 0);
    Close(s);
    return EXIT_SUCCESS;
}
