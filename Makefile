CC = gcc
CFLAGS = -g -Wall -DTRACE
LDIR = lib
ODIR = bin
OBJDIR = obj

LIBS =

CLIENT_TARGET = $(patsubst %.c,%, $(wildcard client_*.c))
SERVER_TARGET = $(patsubst %.c,%, $(wildcard server_*.c))
LIBOBJ = $(patsubst %.c,%.o, $(wildcard $(LDIR)/*.c))
SHARED = client.c service.c
SHARED_OBJECTS = $(LIBOBJ) $(OBJDIR)/protocol_xdr.o $(patsubst %.c,$(OBJDIR)/%.o, $(SHARED))
HEADERS = $(wildcard $(LDIR)/*.h)


.PHONY: default all clean server client

default: server client
all: default

server: $(SERVER_TARGET)
client: $(CLIENT_TARGET)


setup:
	mkdir -p $(ODIR)/server $(ODIR)/client $(OBJDIR)

$(OBJDIR)/%.o: %.c $(HEADERS) | setup
	$(CC) $(CFLAGS) -c $< -o $@

#.PRECIOUS: $(CLIENT_TARGET) $(OBJECTS)

$(CLIENT_TARGET) : % : $(OBJDIR)/%.o $(SHARED_OBJECTS)
	$(CC) $^ $(LIBS) -o $(ODIR)/client/$@

$(SERVER_TARGET) : % : $(OBJDIR)/%.o $(SHARED_OBJECTS)
	$(CC) $^ $(LIBS) -o $(ODIR)/server/$@

clean:
	rm -rf *.o *~ $(LDIR)/*.o $(ODIR) $(OBJDIR)

# create test file to transfer
test:
	# 256 MB file
	head -c 1k </dev/urandom > $(ODIR)/server/bigfile
	dd if=/dev/zero bs=1M count=256 >> $(ODIR)/server/bigfile
	head -c 1k </dev/urandom >> $(ODIR)/server/bigfile
	# text file
	echo "Hello World!\n12345 67890" > $(ODIR)/server/test.txt
