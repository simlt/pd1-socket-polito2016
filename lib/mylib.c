#include "mylib.h"
#include "errlib.h"

#include <errno.h>
#include <sys/sendfile.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>      // creat

#define CHUNK_SIZE    1024
#define PADDING_SIZE  4

extern char* prog_name;


int waitResponse(int socket, void *bufptr, size_t nbytes, int flags, SA *sa, socklen_t *salenptr, long timeout, ssize_t* recvbytes)
{
    struct timeval timeout_s;
    fd_set fds;
    int n;

    timeout_s.tv_sec = timeout;
    timeout_s.tv_usec = 0;

    FD_ZERO(&fds);
    FD_SET(socket, &fds);
    // Note: Select could exit
    n = Select (socket+1, &fds, NULL, NULL, &timeout_s);
    if (n <= 0)
        return 0;
    else
    {
        *recvbytes = Recvfrom(socket, bufptr, nbytes, flags, sa, salenptr);
        return 1;
    }
}

int my_select_socket(int socket)
{
    return my_select_timeout(socket, 0);
}

int my_select_timeout(int socket, long timeout)
{
    struct timeval timeout_s;
    fd_set fds;
    int n;

    timeout_s.tv_sec = timeout;
    timeout_s.tv_usec = 0;

    FD_ZERO(&fds);
    FD_SET(socket, &fds);
    while (1)
    {
        if ((n = select(socket+1, &fds, NULL, NULL, &timeout_s)) < 0)
            if (INTERRUPTED_BY_SIGNAL)
                continue;
        break;
    }
    return n;
}


void fillSockaddr(struct sockaddr_in* saddr, char* address, int port)
{
    saddr->sin_family = AF_INET;
    Inet_aton(address, &saddr->sin_addr);
    saddr->sin_port = htons(port);
}

// use for TCP socket
int my_sendfile(int out_fd, int in_fd, size_t count)
{
    ssize_t sent = 0;
    size_t remaining = count;
    off_t offset = 0;

    // we may receive a SIGPIPE if client closes connection
    // sendfile has no options argument for MSG_NOSIGNAL, so use signal handler
    signal(SIGPIPE, SIG_IGN);

    while (remaining > 0)
    {
        if ((sent = sendfile(out_fd, in_fd, &offset, remaining)) <= 0)
        {
            if (INTERRUPTED_BY_SIGNAL)
            {
                sent = 0;
                continue; /* and call sendfile() again */
            }
            else
                return -1; // Couldn't complete the transfer
        }
        remaining -= sent;
    }
    return count;
}

int my_recvfile(int in_fd, int out_fd, size_t count)
{
    ssize_t recv = 0;
    size_t remaining = count;
    size_t max;
    char* buffer[CHUNK_SIZE];

    // we may receive a SIGPIPE if server closes connection
    while (remaining > 0)
    {
        max = remaining < CHUNK_SIZE ? remaining : CHUNK_SIZE;
        if ((recv = Recv(in_fd, buffer, max, 0)) <= 0)
        {
            if (INTERRUPTED_BY_SIGNAL)
            {
                recv = 0;
                continue; /* and call sendfile() again */
            }
            else
                return -1; // Couldn't complete the transfer
        }
        write(out_fd, buffer, recv);
        remaining -= recv;
    }
    return count;
}


/*
 * TRANSFER FUNCTIONS
 */

// Initialize and return a struct for a pending transfer
struct transfer* my_transfer_init(int in_fd, const char* filename)
{
    struct transfer* t;
    if ((t = malloc(sizeof(struct transfer))) == NULL)
        err_sys("(%s) Malloc failed: not enough memory!", prog_name);
    t->in_fd = in_fd;
    t->out_fd = 0;
    t->in_progress = 0;
    t->remaining = t->file_size = 0;
    if ((t->filename = strdup(filename)) == NULL)
    {
        free(t);
        err_sys("(%s) Malloc failed: not enough memory!", prog_name);
    }
    return t;
}

int my_transfer_started(struct transfer* t)
{
    return t->in_progress == 1;
}

void my_transfer_start(struct transfer* t, size_t file_size)
{
    int fd;
    // Open file to save data
    fd = creat(t->filename, 0644);
    if (fd < 0)
        err_sys("(%s) File creation failed for transfer\n", prog_name);
    t->out_fd = fd;
    t->remaining = t->file_size = file_size;
    t->in_progress = 1;
}

void my_transfer_free(struct transfer* t)
{
    if (t->out_fd)
        close(t->out_fd);
    free(t->filename);
    free(t);
}

int my_transfer_completed(struct transfer* t)
{
    return t->in_progress == 1 && t->remaining == 0;
}


// Not tested yet
/*
int my_transferfile_avoid_block(struct transfer* t, long timeout)
{
    struct timeval timeout_s;
    fd_set rfds;
    fd_set wfds;
    int n;
    int retry = 10;
    ssize_t read_size;
    ssize_t write_size;
    ssize_t remaining;
    size_t offset;

    char* buffer;
    size_t buffer_size;

    timeout_s.tv_sec = timeout;
    timeout_s.tv_usec = 0;

    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    FD_SET(t->in_fd, &rfds);
    FD_SET(t->out_fd, &wfds);
    n = t->in_fd > t->out_fd ? t->in_fd : t->out_fd;

    n = select(n+1, &rfds, &wfds, NULL, &timeout_s);
    if (n < 0)
        return -1;
    else if (n != 2)
        return 0;


    // we can proceed without blocking
    if ((read_size = read(t->in_fd, buffer, buffer_size)) <= 0)
        return -1;
    remaining = read_size;
    write_size = 0;
    offset = 0;
    while((remaining > 0) && (--retry > 0))
    {
        write_size = write(t->out_fd, buffer + offset, remaining);
        if (write_size < 0)
            return -1;
        offset += write_size; // redundant info buf more readable
        remaining -= write_size;
    }
    if (retry <= 0)
    {
        err_sys("(%s) File transfer failed: unable to transfer all data", prog_name);
        return -1;
    }
    t->remaining -= write_size;
    return 1;
}
*/

// RFC 1014 (variable-length opaque data), must send size before this
void xdr_sendfile_buffered(FILE* sockstream, int fd, size_t size)
{
    int i;
    int chunks = size / CHUNK_SIZE;
    size_t last_chunk = size % CHUNK_SIZE;
    int padding_bytes = -size & (PADDING_SIZE-1);
    char buffer[CHUNK_SIZE];

    // Transfer by chunks
    for (i = 0; i < chunks; ++i)
    {
        read(fd, buffer, CHUNK_SIZE);
        fwrite(buffer, CHUNK_SIZE, 1, sockstream);
    }
    // last chunk (if needed)
    if (last_chunk)
    {
        read(fd, buffer, last_chunk);
        fwrite(buffer, last_chunk, 1, sockstream);
    }
    if (padding_bytes > 0)
    {
        *(int*)buffer = 0; // zero out 4 bytes on buffer
        fwrite(buffer, padding_bytes, 1, sockstream);
    }
}
