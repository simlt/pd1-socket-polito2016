/* Author: Simon Mezzomo */
/* Define useful functions for socket programming */

#ifndef _MYLIB_H
#define _MYLIB_H

#include "sockwrap.h"
#include <sys/queue.h> // for lists

/* wait for a recv or return within a timeout (seconds)
   Returns 0 on timeout, 1 if success
   IT CAN exit
*/
int waitResponse(int socket, void *bufptr, size_t nbytes, int flags, SA *sa, socklen_t *salenptr, long timeout, ssize_t* recvbytes);

// Wait for the socket to have some data to read
// Returns value returned by select (-1 on error, >0 if non blocking socket)
int my_select_socket(int socket);

// Wait for the socket to have some data to read until timeout in seconds expires
// Returns value returned by select (0 if timeout expired, -1 on error, >0 if non blocking socket)
int my_select_timeout(int socket, long timeout);

// fill sockaddr_in structure with correct data
void fillSockaddr(struct sockaddr_in* saddr, char* address, int port);

// use for TCP socket (out_fd must be a socket) (transfers count bytes of data from in_fd to out_fd (man sendfile)
// returns bytes sent if success, -1 if failed
int my_sendfile(int out_fd, int in_fd, size_t count);

// use for TCP socket (in_fd) saves the data received on out_fd
int my_recvfile(int in_fd, int out_fd, size_t count);

// Use to send a file of "size" opened on "fd" over an XDR "sockstream" opened with stdio_create
// MUST send the size (uint32_t) before using this
void xdr_sendfile_buffered(FILE* sockstream, int fd, size_t size);


/*
 *  TRANSFER FUNCTIONS
 */
// Transfer status
struct transfer {
    int in_fd;
    int out_fd;
    int in_progress;
    size_t remaining;
    size_t file_size;
    char* filename;
    TAILQ_ENTRY(transfer) transfers; // Used for queues
};

// Initialize and return a struct for a pending transfer
struct transfer* my_transfer_init(int in_fd, const char* filename);

// Set initial file size
void my_transfer_start(struct transfer* t, size_t file_size);

// Return 1 if transfer has started
int my_transfer_started(struct transfer* t);

// Clean up struct
void my_transfer_free(struct transfer* t);

// Return 1 if transfer has completed, 0 otherwise
int my_transfer_completed(struct transfer* t);

// Transfer data from in_fd to out_fd initialized in struct transfer.
// Return 1 on success, 0 if it blocks, -1 on error
int my_transferfile_avoid_block(struct transfer* t, long timeout);

#endif
