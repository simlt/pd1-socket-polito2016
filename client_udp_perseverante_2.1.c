#include "lib/mylib.h"

#include <string.h>
#include <stdlib.h>

#define BUFLEN      32
#define TIMEOUT     3
#define NTRIES      5

char *prog_name; // for errlib


int main(int argc, char** argv)
{
    int udp_socket;
    struct sockaddr_in saddr;
    char buffer[BUFLEN];
    int len;
    socklen_t addrlen;
    ssize_t nresponse;
    int count;

    if (argc != 4)
    {
        printf("Usage: %s address port name \n"
                "(address in dot number form)\n", argv[0]);
        return -1;
    }
    strncpy (buffer, argv[3], BUFLEN);
    buffer[31] = '\0';
    len = strlen(buffer);

	fillSockaddr(&saddr, argv[1], atoi(argv[2]));
	udp_socket = Socket(AF_INET, SOCK_DGRAM, 0);
	// Send payload
	Sendto (udp_socket, buffer, len, 0, (struct sockaddr*)&saddr, sizeof(saddr));
	
	// Wait for response within a timeout
    for (count = 0;;count++)
    {
        if (count == NTRIES) // try again n times
	    {
    	    printf("\nTimeout expired\n");
	        break;
        }
        addrlen = sizeof(saddr);
        if (waitResponse(udp_socket, buffer, BUFLEN, 0, (struct sockaddr*)&saddr, &addrlen, TIMEOUT, &nresponse))
        {
            printf("\nUDP Received %zd bytes. Message: '%s'\n", nresponse, buffer);
            break;
        }
	    // try again
        printf(".");
        fflush(stdout);
    }

	Close(udp_socket);
	return 0;
}

