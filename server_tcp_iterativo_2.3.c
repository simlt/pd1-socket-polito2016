#include "lib/mylib.h"
#include "lib/sockwrap.h"
#include "service.h"

#include <stdlib.h>
#include <unistd.h>


#define LISTEN_BACKLOG  128     // Maximum requests to accept
#define SERVICE_TIMEOUT 30      // Timeout in seconds


char *prog_name; // for errlib

int main(int argc, char** argv)
{
    int tcp_socket, s;
    struct sockaddr_in saddr;
    socklen_t addrlen;
    int port;

    if (argc != 2)
    {
        printf("Usage: %s port\n", argv[0]);
        return -1;
    }
    port = atoi(argv[1]);
    fillSockaddr(&saddr, "0.0.0.0", port);
    tcp_socket = Socket(AF_INET, SOCK_STREAM, 0);
    // Bind to address
    Bind(tcp_socket, (struct sockaddr*)&saddr, sizeof(saddr));
    // Listen
    Listen(tcp_socket, LISTEN_BACKLOG);
    printf("Server started on port %d\n", port);

    // Main loop: never end
    while (1)
    {
        addrlen = sizeof(saddr);
        // Wait for a connection
        s = accept(tcp_socket, (struct sockaddr*)&saddr, &addrlen);
        if (s < 0)
            continue;
        // Service until timeout
        while (1)
        {
            if (my_select_timeout(s, SERVICE_TIMEOUT) > 0)
            {
                if (service(s) >= 0)
                    continue; // OK
            }
            else
                printf("Client connection timed out\n");
            close(s);
            break; // exit loop and close connection
        }
    }
    // Should never reach here
    close(tcp_socket);
    return 0;
}
