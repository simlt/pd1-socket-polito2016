#include "lib/mylib.h"
#include "lib/sockwrap.h"
#include "service.h"

#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>     // signal handler


#define LISTEN_BACKLOG  128     // Maximum requests to accept
#define SERVICE_TIMEOUT 5*60    // Timeout in seconds
#define MAX_SERVICE     3       // Max number of clients to serve concurrently


char *prog_name; // for errlib

static int in_service = 0;

static void child_handler(int sig)
{
    int status;

    while(waitpid(-1, &status, WNOHANG) > 0)
        in_service--;
}

int main(int argc, char** argv)
{
    int tcp_socket, s;
    struct sockaddr_in saddr;
    socklen_t addrlen;
    int port;

    struct sigaction sa;
    sigset_t mask;

    prog_name = argv[0];

    if (argc != 2)
    {
        printf("Usage: %s port\n", argv[0]);
        return -1;
    }
    port = atoi(argv[1]);
    fillSockaddr(&saddr, "0.0.0.0", port);
    tcp_socket = Socket(AF_INET, SOCK_STREAM, 0);
    // Bind to address
    Bind(tcp_socket, (struct sockaddr*)&saddr, sizeof(saddr));
    // Listen
    Listen(tcp_socket, LISTEN_BACKLOG);
    printf("Server started on port %d\n", port);


    /* Establish SIGCHLD handler. */
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = child_handler;
    sigaction(SIGCHLD, &sa, NULL);
    // prepare mask to block SIGCHLD
    sigemptyset(&mask);
    sigaddset(&mask, SIGCHLD);


    // Main loop: never end
    while (1)
    {
        addrlen = sizeof(saddr);

        // check if we have reached max num of active services
        // if yes, block process until a child exits
        // Avoid race condition if signal get's handled before reaching the wait in case we already entered the while loop
        sigprocmask(SIG_BLOCK, &mask, NULL);
        while (in_service >= MAX_SERVICE)
        {
            wait(NULL); // wait for a child to terminate
            in_service--;
        }
        sigprocmask(SIG_UNBLOCK, &mask, NULL);

        // Wait for a connection
        s = Accept(tcp_socket, (struct sockaddr*)&saddr, &addrlen);
        if (s < 0)
            continue;
        // Service until timeout
        if (Fork() == 0)
        {
            // CHILD
            close(tcp_socket);
            while (1)
            {
                if (my_select_timeout(s, SERVICE_TIMEOUT) > 0)
                {
                    if (service(s) >= 0)
                        continue; // OK
                }
                else
                    printf("Client connection timed out\n");

                // exit and close connection
                close(s);
                exit(EXIT_SUCCESS); // terminate child process
            }
        }
        // Only parent reaches here
        // add 1 to in service counter
        in_service++;
    }
    // Should never reach here
    close(tcp_socket);
    return 0;
}
