#include "service.h"
#include "lib/mylib.h"

#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "protocol.h" // XDR header


#define BUFLEN          256
#define MAX_FILENAME    100
#define CHUNK_SIZE      256     // MUST BE >=4! Used to split file in chunks of byte size


static const char MSG_GET[] = "GET ";
static const char MSG_OK[] = "+OK\r\n";
static const char MSG_ERR[] = "-ERR\r\n";
static const char MSG_QUIT[] = "QUIT\r\n";


int service(int socket)
{
    char buffer[BUFLEN];
    uint32_t* ptr;
    char filename[MAX_FILENAME];
    ssize_t nread;
    int size;
    size_t filesize;
    int fd;
    struct stat filestat;
    int error = 0;

    // Read data (buffered!)
    nread = readline(socket, buffer, BUFLEN);
    if (nread == 0) // Socket has been closed on other side
        return -1;

    printf("TCP server message received %zd bytes.\n", nread);

    // Check MESSAGE
    if (strncmp(buffer, MSG_GET, strlen(MSG_GET)) == 0)
    {
        if (sscanf(buffer, "GET %99s[^\r]\n", filename) != 1) // read 100-1 chars
        {
            printf("Invalid GET message format received\n");
            error = 1;
        }
        printf("GET request: %s\n", filename);
    }
    else if (strncmp(buffer, MSG_QUIT, strlen(MSG_QUIT)) == 0)
    {
        printf("QUIT request\n"
               "Closing connection\n");
        return -1; // Quit
    }
    else
    {
        printf("Unknown message format received\n");
        error = 1;
    }


    // Try to open the requested file (in current working dir)
    fd = open(filename, O_RDONLY);
    if (fd < 0)
    {
        printf("File %s not found\n", filename);
        error = 1;
    }
    // Get stat
    if (fstat(fd, &filestat) < 0)
    {
        printf("Stat on file failed!\n");
        error = 1;
    }

    if (error)
    {
        // Send err message
        printf("Sending error message\n");
        sendn(socket, MSG_ERR, strlen(MSG_ERR), MSG_NOSIGNAL);
        return 0; // wait for new request
    }

    // Create response (+OK\r\nFILESIZE|MODIFY_TIME)
    filesize = filestat.st_size;
    strcpy(buffer, MSG_OK);
    ptr = (uint32_t*)(buffer + strlen(MSG_OK));
    *ptr = htonl(filesize);
    ptr++; // Next field
    *ptr = htonl(filestat.st_mtime);
    size = strlen(MSG_OK) + 2*sizeof(uint32_t); // string + uint32_t + uint32_t

    // Send response header
    if (sendn(socket, buffer, size, MSG_NOSIGNAL) < 0)
    {
        close(fd);
        return -1; // Something went wrong: close connection
    }
    // Send file
    my_sendfile(socket, fd, filesize);
    // SIMULATE SERVICE DELAY
    //sleep(10); // sleep 10 seconds

    close(fd);
    // All went fine, wait for a new request
    printf("File successfully sent.\n");
    return 1;
}


int service_xdr(int socket)
{
    int fdfile;
    int fdsock;
    struct stat filestat;
    size_t size;
    int error = 0;

    // XDR
    XDR xdrs;
    // struct file xdrfile;
    message client_message;
    message server_message;
    FILE* fsocket;
    //file* xdrfilep;
    tagtype xdrtag;

    // copy the fd and use it to open TCP stream as stdio (this is done to be able to close the file without closing the original socket)
    if ((fdsock = dup(socket)) < 0)
    {
        printf("Unable to copy file descriptor of socket\n");
        return -1;
    }
    if ((fsocket = fdopen(fdsock, "r+")) == NULL)
    {
        printf("Unable to open stream on socket\n");
        close(fdsock);
        return -1;
    }

    // Create XDR on socket file (remember to only use fwrite/fread is needed because of buffer)
    xdrstdio_create(&xdrs, fsocket, XDR_DECODE);

    // Clear structs
    memset(&client_message, 0, sizeof(message));
    memset(&server_message, 0, sizeof(message));

    // Read message
    if (!xdr_message(&xdrs, &client_message))
    {
        printf("Invalid message received\n");
        xdr_destroy(&xdrs);
        fclose(fsocket);
        return -1; // Close service
    }

    printf("TCP server message received.\n");

    // Check MESSAGE
    switch (client_message.tag)
    {
    case GET:
        printf("GET request for file: %s\n", client_message.message_u.filename);
        break;
    case QUIT:
        printf("QUIT request\n");
        error = 1;
    default:
        printf("Unknown message format received\n");
        error = 1;
    }
    // Error or quit request (close service)
    if (error)
    {
        xdr_free((xdrproc_t)xdr_message, (char*)&client_message);
        xdr_destroy(&xdrs);
        fclose(fsocket);
        return -1; // Close service
    }

    // Try to open the requested file (in current working dir)
    fdfile = open(client_message.message_u.filename, O_RDONLY);
    if (fdfile < 0)
    {
        printf("File %s not found\n", client_message.message_u.filename);
        error = 1;
    }
    // Get file stat
    else if (fstat(fdfile, &filestat) < 0)
    {
        printf("Stat on file failed!\n");
        error = 1;
        close(fdfile);
    }

    // End reading data and start response sending
    xdr_destroy(&xdrs);
    xdrstdio_create(&xdrs, fsocket, XDR_ENCODE);

    if (!error)
    {
/* Avoid using malloc, but instead send by chunks
        xdrfilep = &server_message.message_u.fdata;
        size = filestat.st_size;
        // Create response header (OK)
        server_message.tag = OK;
        xdrfilep->last_mod_time = filestat.st_mtime;
        // Send file
        filedata = malloc(size);
        read(fd, filedata, size); // don't check for now
        xdrfilep->contents.contents_len = size;
        xdrfilep->contents.contents_val = filedata;

        // send data
        xdr_message(&xdrs, &server_message);
        free(filedata);
*/
        xdrtag = OK;
        xdr_tagtype(&xdrs, &xdrtag);
        // Send file size + data (xdr_bytes) manually
        size = filestat.st_size;
        xdr_u_int(&xdrs, (unsigned int*)&size);
        // Send file (use buffered write because of xdrstdio_create)
        xdr_sendfile_buffered(fsocket, fdfile, size);
        // Send creation time
        xdr_u_int(&xdrs, (unsigned int*)&filestat.st_mtime);
        // close requested file
        close(fdfile);
    }
    else // error
    {
        // SEND error message
        server_message.tag = ERR;
        xdr_message(&xdrs, &server_message);
    }

    // SIMULATE SERVICE DELAY
    //sleep(10); // sleep 10 seconds

    // Remember to free ALL XDR structs
    xdr_free((xdrproc_t)xdr_message, (char*)&client_message);
    xdr_free((xdrproc_t)xdr_message, (char*)&server_message);
    xdr_destroy(&xdrs);

    // Close the FILE of the copy of socket file descriptor
    fclose(fsocket);
    // Request was correctly handled
    printf("File successfully sent.\n");
    return 1;
}
