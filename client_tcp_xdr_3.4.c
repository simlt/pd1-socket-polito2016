#include "client.h"

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

char *prog_name; // for errlib


int main (int argc, char** argv)
{
    int s;
    struct sockaddr_in saddr;
    size_t addrlen;
    int option_xdr = 0;

    prog_name = argv[0];

    if (argc < 3 || argc > 4)
    {
        printf("%s [-x] <dotted-address> <port>\n", argv[0]);
        return EXIT_FAILURE;
    }
    if (argc == 4 && strcmp(argv[1], "-x") == 0)
        option_xdr = 1;

    s = Socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    fillSockaddr(&saddr, argv[1 + option_xdr], atoi(argv[2 + option_xdr]));
    addrlen = sizeof(saddr);
    Connect(s, (struct sockaddr*)&saddr, addrlen);
    printf("Connected!\n");
    client(s, option_xdr);
    Close(s);
    return EXIT_SUCCESS;
}
