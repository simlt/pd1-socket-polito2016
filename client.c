#include "client.h"

#include <string.h>
#include <sys/queue.h>
#include <assert.h>

#include "protocol.h" // XDR header


#define MAX_FILENAME    100
#define BUFSIZE         256   // must be > MAX_FILENAME + 20

static const char MSG_GET[] = "GET ";
static const char MSG_OK[] = "+OK\r\n";
static const char MSG_ERR[] = "-ERR\r\n";

#define OK_HEADER_SIZE       (strlen(MSG_OK) + 2*sizeof(uint32_t))


enum cmd {
    CMD_GET,
    CMD_QUIT,
    CMD_ABORT,
    CMD_ERROR,
};

enum response {
    COMPLETED,
    PROGRESS,
    NOT_FOUND,
    RESP_ERROR,
    PARTIAL_HEADER
};


void client(int s, int xdr)
{
    char filename[MAX_FILENAME];
    fd_set fds;
    const int fdstdin = fileno(stdin);
    const int maxfd = fdstdin > s ? fdstdin : s;
    int quitting = 0;
    struct transfer *cur_transfer = NULL, *new_transfer = NULL;
    char buffer[BUFSIZE];
    int result;

    // Queue of transfers
    TAILQ_HEAD(listhead, transfer) transfer_queue;
    TAILQ_INIT(&transfer_queue);

    printf("Client connected to server. Commands: GET, QUIT, ABORT\n");
    printf("\n> ");
    fflush(stdout);

    while (1)
    {
        // Get transfer on head of queue
        cur_transfer = transfer_queue.tqh_first;

        // Check if there is a pending input available to process
        FD_ZERO(&fds);
        FD_SET(s, &fds);
        FD_SET(fdstdin, &fds);

        // Check if no transfer is in progress and we have to quit
        if (quitting == 1 && cur_transfer == NULL)
        return; // quit

        // wait for some input to be ready
        Select(maxfd+1, &fds, NULL, NULL, NULL);

        // Check if input from stdio is ready
        if (FD_ISSET(fdstdin, &fds) && !quitting)
        {
            if (fgets(buffer, BUFSIZE, stdin) == NULL)
            continue;

            switch (parseCommand(buffer))
            {
                case CMD_GET:
                    if (sscanf(buffer, "GET %99s", filename) != 1)  // MAX_FILENAME
                    {
                        printf("Invalid command. Syntax: GET filename.txt\n");
                        break;
                    }
                    new_transfer = queueTransfer(s, filename);
                    TAILQ_INSERT_TAIL(&transfer_queue, new_transfer, transfers);
                    cur_transfer = transfer_queue.tqh_first;
                    // If no transfers are in progress, start it immediately
                    if (cur_transfer != NULL && !my_transfer_started(cur_transfer))
                    requestTransfer(cur_transfer);
                    break;
                case CMD_QUIT:
                    quitting = 1;
                    break;
                case CMD_ABORT:
                    quitting = 1;
                    return; // quit immediately
                case CMD_ERROR:
                    default:
                    break; // do nothing
            }
            //fflush(stdin);
            printf("\n> ");
            fflush(stdout);
        }
        // Check if data from socket is ready
        if (FD_ISSET(s, &fds))
        {
            // parse received data
            if (xdr)
                result = parseResponse_xdr(s, cur_transfer);
            else
                result = parseResponse(s, cur_transfer);

            switch (result)
            {
                case NOT_FOUND:
                    // Pop current transfer from queue
                    printf("Removing current invalid transfer\n");
                    // don't break here
                case COMPLETED:
                    TAILQ_REMOVE(&transfer_queue, cur_transfer, transfers);
                    my_transfer_free(cur_transfer);
                    // Check if we have a new transfer to start
                    if ((cur_transfer = transfer_queue.tqh_first) != NULL)
                    requestTransfer(cur_transfer);
                    break;
                case PROGRESS:
                case PARTIAL_HEADER:
                    break; // do nothing, just cycle again when new data is ready
                case RESP_ERROR:
                    // end client service
                    return;
            }
        }
    }
}


int parseResponse(int s, struct transfer* t)
{
    static char resp_buffer[BUFSIZE];
    static size_t ready = 0;

    size_t max_readable = BUFSIZE;
    size_t size;
    ssize_t r;
    uint32_t filesize;
    uint32_t modtime;

    // Make sure we don't read extra data
    if (t && my_transfer_started(t))
    {
        if (t->remaining < max_readable)
            max_readable = t->remaining;
    }
    else
    {
        // We still have to read header data (worst case is ok header, there may be a problem if err header if followed by another err header and this function is not called again)
        max_readable = OK_HEADER_SIZE;
    }
    // Read only extra data not already in buffer
    max_readable -= ready;
    // Read and append data in buffer
    r = Recv(s, resp_buffer + ready, max_readable, 0);
    if (r == 0)
    {
        printf("Connection closed by server\n");
        return RESP_ERROR;
    }
    // update ready data count
    ready += r;
    assert(ready <= BUFSIZE);

    // Handle file transfer (if there is one)
    if (t && my_transfer_started(t))
    {
        size = t->remaining < ready ? t->remaining : ready;
        Write(t->out_fd, resp_buffer, size);
        consumeBuffer(resp_buffer, &ready, size);
        t->remaining -= size;
        if (my_transfer_completed(t))
        {
            printf("Current transfer complete. Written %zu bytes of data\n", t->file_size);
            return COMPLETED;
        }
        return PROGRESS;
    }

    // Otherwise the transfer must be started
    if (strncmp(resp_buffer, MSG_OK, strlen(MSG_OK)) == 0)
    {
        // OK
        assert(t); // a current transfer must exist
        // check if we have all header data, if not wait for it
        if (ready < OK_HEADER_SIZE)
            return PARTIAL_HEADER;

        filesize = *((uint32_t*)(resp_buffer + strlen(MSG_OK)));
        modtime = *((uint32_t*)(resp_buffer + strlen(MSG_OK) + sizeof(uint32_t)));
        filesize = ntohl(filesize);
        modtime = ntohl(modtime);
        consumeBuffer(resp_buffer, &ready, OK_HEADER_SIZE);

        // setup to start transferring the data in the next call
        printf("Starting transfer. Size: %d Last mod: %d\n", filesize, modtime);
        my_transfer_start(t, filesize);

        // check if a 0 byte file was already received
        if (my_transfer_completed(t))
        {
            printf("Current transfer complete. Written %zu bytes of data\n", t->file_size);
            return COMPLETED;
        }
        return PROGRESS;
    }
    else if (strncmp(resp_buffer, MSG_ERR, strlen(MSG_ERR)) == 0)
    {
        // ERROR
        printf("ERROR: Server failed to serve request\n");
        consumeBuffer(resp_buffer, &ready, strlen(MSG_ERR));
        return NOT_FOUND;
    }

    printf("Unhandled response format!\n");
    return RESP_ERROR;
}


int parseResponse_xdr(int s, struct transfer* t)
{
    printf("TODO!\n");
    return RESP_ERROR;
}


int parseCommand(char* cmdstring)
{
    // QUIT after transfer
    if (cmdstring[0] == 'Q')
    {
        puts("Quitting after transfers complete...\n");
        return CMD_QUIT;
    }
    // ABORT (quit immediately)
    else if (cmdstring[0] == 'A')
    {
        puts("Quitting.\n");
        return CMD_ABORT;
    }
    // GET command
    else if (strncmp(cmdstring, MSG_GET, strlen(MSG_GET)) == 0)
    {
        return CMD_GET;
    }

    puts("Invalid command\n");
    return CMD_ERROR;
}


struct transfer* queueTransfer(int s, const char* filename)
{
    struct transfer *t;

    printf("Queueing file transfer of %s\n", filename);

    t = my_transfer_init(s, filename);

    return t;
}

void requestTransfer(struct transfer* t)
{
    char buffer[MAX_FILENAME + 20];
    printf("Starting new file transfer\n");

    // Prepare the request message
    snprintf(buffer, BUFSIZE, "GET %s\r\n", t->filename);
    // Send the message we prepared to the server
    Sendn(t->in_fd, buffer, strlen(buffer), 0);
}

void consumeBuffer(char* buffer, size_t* ready, size_t size)
{
    unsigned int dst;
    unsigned int src = (unsigned int)size;

    // move data to beginning
    for (dst = 0; dst < size; ++dst, ++src)
        buffer[dst] = buffer[src];

    *ready = *ready - size;
}
