#include "lib/mylib.h"

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <rpc/xdr.h>


#define BUFSIZE 32

char *prog_name; // for errlib



int getResponse(int s, uint16_t a, uint16_t b, uint16_t* res)
{
    int n, pos;
    unsigned int temp;
    char buf[BUFSIZE];
    XDR xdrs;

    /* create XDR stream */
    xdrmem_create(&xdrs, buf, BUFSIZE, XDR_ENCODE);
    temp = a;
    xdr_u_int(&xdrs, &temp);
    temp = b;
    xdr_u_int(&xdrs, &temp);
    pos = xdr_getpos(&xdrs);
    Sendn(s, buf, pos, 0);
    xdr_destroy(&xdrs);

    n = Recv(s, buf, BUFSIZE, 0);
    if (n == 0)
    {
        puts("Connection closed by server\n");
        return -1;
    }
    xdrmem_create(&xdrs, buf, BUFSIZE, XDR_DECODE);
    xdr_u_int(&xdrs, &temp);
    *res = (uint16_t)temp; // may be truncated if res >= 2^16
    xdr_destroy(&xdrs);
    return 0;
}

int main (int argc, char** argv)
{
    int s;
    uint16_t a, b, res;
    struct sockaddr_in saddr;
    size_t addrlen;

    prog_name = argv[0];

    if (argc != 3)
    {
        printf("%s <dotted-address> <port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    s = Socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
/*  if (s < 0)
    {
        puts("Could not open socket\n");
        exit(EXIT_FAILURE);
    }
*/ 

    fillSockaddr(&saddr, argv[1], atoi(argv[2]));
    addrlen = sizeof(saddr);
    Connect(s, (struct sockaddr*)&saddr, addrlen);
    printf("Connected!\n");
    while(1)
    {
        printf("> ");
        if (scanf("%hu %hu", &a, &b) != 2)
            continue;
        if (getResponse(s, a, b, &res) < 0) 
            break;
        printf(" %d\n", res);
    }
    close(s);
    return EXIT_SUCCESS;
}

