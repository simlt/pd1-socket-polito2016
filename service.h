#ifndef _SERVICE_FILE_H
#define _SERVICE_FILE_H


/* Receive and parse request on socket and if it's valid it sends back the requested file.
 * Returns:
 *  -1 on error (close connection),
 *  0 if no need to close connection,
 *  1 on success
 */
int service(int socket);
int service_xdr(int socket);


#endif /* !_SERVICE_FILE_H */
