#include "lib/mylib.h"

#include <string.h>
#include <stdlib.h>

#define BUFLEN  32
#define TIMEOUT 5

#define CLIENT_QUEUE_ENTRIES    10
#define CLIENT_QUEUE_SIZE       CLIENT_QUEUE_ENTRIES + 1
#define CLIENT_MAX_REQUESTS     4

char *prog_name; // for errlib

typedef struct {
    struct in_addr addr;
    int count;
} client_info_t;

typedef struct {
    client_info_t data[CLIENT_QUEUE_SIZE];
    int head;
    int tail;
} client_queue;


void initQueue(client_queue* queue)
{
    queue->head = 0;
    queue->tail = 0;
}

// Return 1 if the request is valid, 0 if the request already reached maximum request limit
int checkRequest(client_queue* queue, struct in_addr* saddr)
{
    client_info_t* itr;
    int i;
    // search entry
    for (i = queue->head; i != queue->tail; i = (i + 1) % CLIENT_QUEUE_SIZE)
    {
        itr = &queue->data[i];
        if (itr->addr.s_addr == saddr->s_addr) // Match
            if (++itr->count > CLIENT_MAX_REQUESTS)
                return 0;
    }
    // if not found, add an entry and eventually remove last entry
    itr = &queue->data[queue->tail];
    itr->addr = *saddr;
    itr->count = 1;
    queue->tail = (queue->tail + 1) % CLIENT_QUEUE_SIZE;
    if (queue->tail == queue->head) // we added an element, but queue is full
        queue->head = (queue->head + 1) % CLIENT_QUEUE_SIZE;
    return 1;
}


int main(int argc, char** argv)
{
    int udp_socket;
    struct sockaddr_in saddr;
    char buffer[BUFLEN];
    socklen_t addrlen;
    ssize_t nresponse;
    int port;
    client_queue queue;

    initQueue(&queue);

    if (argc != 3)
    {
        printf("Usage: %s address port (address in dot number form)\n", argv[0]);
        return -1;
    }
    port = atoi(argv[2]);
    fillSockaddr(&saddr, argv[1], port);
    udp_socket = Socket(AF_INET, SOCK_DGRAM, 0);
    // Bind to address
    Bind(udp_socket, (struct sockaddr*)&saddr, sizeof(saddr));
    printf("Server started on %s:%d\n", argv[1], port);

    while (1)
    {
        addrlen = sizeof(saddr);
        // Wait for a request
        nresponse = Recvfrom(udp_socket, buffer, BUFLEN, 0, (struct sockaddr*)&saddr, &addrlen);
        if (!checkRequest(&queue, &saddr.sin_addr))
        {
            printf("Connection refused from %d\n", saddr.sin_addr.s_addr);
            continue;
        }
        if (nresponse < BUFLEN)
            buffer[nresponse] = '\0';
        else
            buffer[BUFLEN-1] = '\0';
        printf("UDP server received %zd bytes. Message: '%s'\n", nresponse, buffer);
        // Send response
        Sendto(udp_socket, buffer, nresponse, 0, (struct sockaddr*)&saddr, sizeof(saddr));
    }

    Close(udp_socket);
    return 0;
}

